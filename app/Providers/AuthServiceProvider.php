<?php

namespace App\Providers;

use App\User;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        $this->registerPassport();
    }

    private function registerPassport(): void
    {
        Passport::routes(function ($router) {
            $router->forAccessTokens();
        });

        Passport::tokensCan([
            'create-client' => 'Create clients',
            'create-project' => 'Create projects',
            'update-client' => 'Update clients',
            'update-project' => 'Update projects',
        ]);

        Passport::enableImplicitGrant();

        Passport::tokensExpireIn(now()->addDays(15));

        Passport::refreshTokensExpireIn(now()->addDays(30));
    }
}
