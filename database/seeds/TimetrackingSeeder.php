<?php

use Illuminate\Database\Seeder;
use App\Timetracking;
use App\Project;

class TimetrackingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('timetrackings')->truncate();

        Project::get()->each(function ($p) {
            $p->timetracking()->save(factory(Timetracking::class)->make());
        });
    }
}
