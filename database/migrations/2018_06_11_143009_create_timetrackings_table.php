<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTimetrackingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('timetrackings', function (Blueprint $table) {
            $table->increments('id');
            $table->text('description');
            $table->dateTime('start');
            $table->dateTime('end');
            $table->tinyInteger('user_id');
            $table->tinyInteger('project_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('timetrackings');
    }
}
