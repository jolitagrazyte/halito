<?php

namespace App\Http\Controllers;

use App\Client;
use App\User;

class ApiController extends Controller
{
    private $client;

    /**
     * ApiController constructor.
     *
     * @param \App\Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
        //$this->middleware('auth:api');
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function getClientById(int $id)
    {
        return $this->client->find($id)->first();
    }

    /**
     * @param int $id
     *
     * @return mixed
     */
    public function getProjectById(int $id)
    {
        return Project::find($id)->first();
    }

    /**
     * @return mixed
     */
    public function getClients()
    {
        return $this->client->get();
    }

    /**
     * @param int|NULL $id
     *
     * @return mixed
     */
    public function getClientsForUser(int $id = null)
    {
        return User::find($id)->clients;

    }

    /**
     * @param      $action
     * @param      $entity
     * @param null $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addEditForm($action, $entity, $id = null)
    {
        $action = ucfirst($action);
        return view('add-edit-form', compact('entity', 'id', 'action'));
    }

}
