import formHelper from './components/FormHelper.vue'
import projectsList from './components/ProjectsList.vue'
import invoices from './components/Invoices.vue'
import clientForm from './components/_ClientForm.vue'
import projectForm from './components/_ProjectForm.vue'

export default {
    props: ['href'],
    components: {
        'form-helper': formHelper,
        'projects-list': projectsList,
        'invoices': invoices,
        'client-form': clientForm,
        'project-form': projectForm
    },
    data: function() {
        return {
            name: 'Clients',
            clients: [],
            userData: [],
            activeClient:false,
            activeCProject:false,
            active: false,
            isAdmin: false
        }
    },
    mounted: function() {
        this.getUser();
    },
    methods: {
        getUser: function() {
            axios.get('/api/user')
                .then(response => {
                    //console.log('RESPONSE: '+ response.data);
                    this.userData = response.data;
                    if(this.userData.is_admin){
                        //console.log('isADMIN: TRUE');
                        this.isAdmin = true;
                        this.getAllClients();
                    }
                    else{
                        //console.log('isADMIN: FALSE');
                        this.getClientsForUser(this.userData.id)
                    }
                })
                .catch(error => this.getErrors(error));
        },

        getAllClients: function() {
            axios.get('/api/all-clients')
                .then(response => this.clients = response.data)
                .catch(error => this.getErrors(error));
        },

        getClientsForUser: function(id) {
            axios.get('/api/clients-for-user/'+id)
                .then(response => this.clients = response.data)
                .catch(error => this.getErrors(error));
        },

        getProject: function(id) {
            axios.get('/project/'+id)
                .then(response => console.log(response.data))
                .catch(error => this.getErrors(error));
        },

        submitChanges: function(id) {
            axios.post('/client/'+id+'/')
        },

        getErrors: function(error){
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                console.log(error.response.data);
                console.log(error.response.status);
                console.log(error.response.headers);
            } else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log(error.request);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.log('Error', error.message);
            }
            console.log(error.config);
        }
    }
}
