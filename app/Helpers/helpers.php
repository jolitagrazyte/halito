<?php

function current_user()
{
    return auth()->user();
}

function is_admin()
{
    return current_user()->is_admin === 1;
}

function current_user_name()
{
    return current_user()->first_name.' '.current_user()->last_name;
}

