<?php

use Illuminate\Database\Seeder;
use App\Client;

class ClientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Client::class, 10)->create()->each(function ($c) {
            $c->projects()->save(factory(App\Project::class)->make());
            $c->projects()->save(factory(App\Project::class)->make());
        });
    }
}
