<?php

use Faker\Generator as Faker;
use App\Project;

$factory->define(Project::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'budget' => $faker->numberBetween(500,100000),
        'author_id' => $faker->numberBetween(1,6)
    ];

});
