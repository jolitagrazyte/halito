@extends('layouts.invoice')

@section('page-name', 'Invoice')

@section('style')
    @parent
    <style>
        .page-break {
            page-break-after: always;
        }
    </style>
@endsection

@section('content')
    <h1>{{ 'Invoice Nr.: 000'.$invoice_nr }}</h1>
    <h2>{{ 'Project name: '.$proj_name }}</h2>
    <div>
        <strong>
            Project name:
        </strong>
        <em>
            {{ $client }}
        </em>
    </div>
    <div>
        <strong>
            Email:
        </strong>
        <em>
            {{ $email }}
        </em>
    </div>
    <div>
        <strong>
            Budget:
        </strong>
        <em>
            {{ '€ '.$proj_budget.'.00' }}
        </em>
    </div>
    <div class="page-break"></div>
    <h1>Page 2</h1>
@endsection
