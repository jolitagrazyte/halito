<?php

namespace App\Http\Controllers;

use App\Client;
use App\Project;
use App\User;
use Barryvdh\DomPDF\PDF;

class AppController extends Controller
{
    private $client;

    /**
     * ClientsController constructor.
     *
     * @param \App\Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('clients.index');
    }

    public function store()
    {
        dd(request());

        return $this->client->save(request()->all());
    }

    public function update(int $id = NULL)
    {
        dd($this->client->find($id));
        $this->client->find($id);

        return $this->client->update(request()->all());
    }

    public function projects(int $client_ID = NULL)
    {
        return $this->client->find($client_ID)->projects()->get();
    }

    public function getProjectID(int $client_ID = NULL)
    {
        return $this->projects($client_ID)->map(function ( $project ) {
            return $project->id;
        });
    }

    public function invoices(int $client_ID = NULL)
    {
        $client = $this->client->find($client_ID);

        return $client->projects()->get()->map(function ( $project ) use ( $client ) {
            return $this->getInvoiceData( $client, $project );
        });
    }

    public function invoicePDF(int $client_ID = NULL, int $project_ID = NULL)
    {
        $client = $this->client->find( $client_ID );
        $project = $client->projects()->find( $project_ID );
        $data = $this->getInvoiceData( $client, $project );

        $pdf = app()->make(PDF::class)->loadView('invoice', $data);
        return $pdf->download('invoice.pdf');

    }

    protected function getInvoiceData($client, $project) : array
    {
        return [
            'invoice_nr' => $project->id,
            'proj_name' => $project->name,
            'client' => $client->name,
            'email' => $client->email,
            'proj_budget' => $project->budget,
        ];
    }
}
