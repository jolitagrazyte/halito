<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Timetracking extends Model
{
    protected $guarded = [];
    protected $table = 'timetrackings';
    protected $with = [
      'user'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
