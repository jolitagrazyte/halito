<?php

use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();

        DB::table('users')->insert([
            'first_name' => 'Jolita',
            'last_name' => 'Grazyte',
            'email' => 'jolita@jolitagraze.com',
            'password' => Hash::make('secret'),
            'is_admin' => 1
        ]);

        User::find(1)->createToken('Jolita PToken', [
            'create-client',
            'create-project',
            'update-client',
            'update-project'
        ])->accessToken;

        factory(User::class, 5)->create();
    }
}
