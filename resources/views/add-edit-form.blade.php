@extends('layouts.app')

@section('page-name', 'Clients')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="mt-5">
                        <div id="add-edit-form">
                            @if($entity === 'client')
                                <h4>{{ $action  }} client</h4>
                                <client-form></client-form>
                            @else
                                <h4>{{ $action  }} project</h4>
                                <project-form></project-form>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
