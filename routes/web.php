<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function (){
    return redirect('clients');
});

//Route::resource('/clients','AppController@index');

Route::get('/clients', 'AppController@index')->name('clients');

Route::post('/clients/update/{id}', 'AppController@update')->name('clients');

Route::get('/form/{action}/{entity}/{id?}', 'AppController@addEditForm')->name('addEditForm');

Route::get('/invoice-pdf/{client_id}/{project_id}', 'AppController@invoicePDF');

Route::get('/invoices-link/{client_id}', 'AppController@getProjectID');

Route::get('/projects/{client_id}', 'AppController@projects');

Route::get('/invoices/{client_id}', 'AppController@invoices');
