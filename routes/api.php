<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function () {

    Route::get('/user', function () {
        return request()->user();
    });

    Route::get('/all-clients', 'ApiController@getClients');
    Route::get('/clients-for-user/{id}', 'ApiController@getClientsForUser');
    Route::get('/client/{id}', 'ApiController@getClientById');
    Route::get('/project/{id}', 'ApiController@getProjectById');

});
