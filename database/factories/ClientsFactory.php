<?php

use Faker\Generator as Faker;
use App\Client;

$factory->define(Client::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'telephone_nr' => $faker->phoneNumber,
        'address' => $faker->address,
        'author_id' => $faker->numberBetween(1,6)
    ];
});
