<?php

use Faker\Generator as Faker;
use App\Timetracking;

$factory->define(Timetracking::class, function (Faker $faker) {
    return [
        'description' => $faker->text,
        'start' => $faker->dateTimeBetween($startDate = '-1 years', $endDate = 'now', $timezone = null),
        'end' => $faker->dateTimeBetween($startDate = 'now', $endDate = '+1 years', $timezone = null),
        'user_id' => $faker->numberBetween(1, 5)
    ];
});
